class LCG:
    rand = 0
    a = 1103515245
    c = 12345
    m = 2**32
    
    def __init__(self, initVal):
        self.rand = initVal

    def get_rand(self):
        ans = self.rand
        self.rand = ((self.a * self.rand + self.c) % self.m)
        return (ans >> 16) % (2**15)