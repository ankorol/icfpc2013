from base_classes import *
from backtracker import *
import rnd
import json
import ConfigParser

if __name__ == "__main__":
    dic = json.load(file("C:\\Programming\\ICFPC2013\\2015\\qual_problems\\input\\problem_1.json"))
    ans = []
    for i in dic['sourceSeeds']:
        board = Board.from_dict(dic['height'], dic['width'], dic['filled'])
        r = rnd.LCG(i)
        a, b = solve(board, dic, r)
        print a, b
        ans.append({'problemId': dic['id'], 'seed': i, 'solution': b})
    json.dump(ans, file("C:\\Programming\\ICFPC2013\\2015\\qual_problems\\problem_1_ans.json", 'w'))