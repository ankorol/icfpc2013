from collections import deque
from base_classes import *
def pos_to_str(pos):
    return pos[0].to_string() + ' ' + str(pos[1])

def moveE(pos):
    return (Cell(pos[0].row, pos[0].column + 1), pos[1])
    
def moveW(pos):
    return (Cell(pos[0].row, pos[0].column - 1), pos[1])

def moveSE(pos):
    if pos[0].row % 2 == 0:
        return (Cell(pos[0].row + 1, pos[0].column), pos[1])
    else:
        return (Cell(pos[0].row + 1, pos[0].column + 1), pos[1])
    
def moveSW(pos):
    if pos[0].row % 2 == 0:
        return (Cell(pos[0].row + 1, pos[0].column - 1), pos[1])
    else:
        return (Cell(pos[0].row + 1, pos[0].column), pos[1])


def turn_clock(pos):
    return (Cell(pos[0].row, pos[0].column), (pos[1] + 300) % 360)

def turn_conterclock(pos):
    return (Cell(pos[0].row, pos[0].column), (pos[1] + 60) % 360)
    
move = [(moveSE, '5'),
        (moveE, 'e'),
        (moveW, '!'),
        (moveSW, 'a'),
        (turn_clock, 'd'),
        (turn_conterclock, 'k')]

#move = [(moveE, 'e')]

def getKey(item):
    return item[1] + item[3] * 10

def solve(board, dic, lcg): #return (move_score, moves)
    possible_states = [(board,0, "", 0)]
    kol = 0
    while True and kol < dic['sourceLength']:
        kol += 1
        new_unit_number = lcg.get_rand() % len(dic['units'])
        print "Now generated ", new_unit_number
        unit = Unit(dic['units'][new_unit_number], board.height, board.width)
        new_possible_states = []
        for brd, move_score, moves, ls_old in possible_states:
            if brd.is_allowed(unit, (unit.pivot, 0)): # spawn new unit, it is already ready to spawn
                new_possible_states += solve_for_one_unit(brd, moves, move_score, unit, (unit.pivot, 0), ls_old)
        if (len(new_possible_states) == 0):
            break
        possible_states = new_possible_states
        possible_states.sort(key=getKey)
        possible_states = possible_states[-5:]
    if len(possible_states) == 0: return (0, "")
    mx = possible_states[0][1]
    moves = possible_states[0][2]
    for pos_state in possible_states:
        if mx < pos_state[1]:
            mx = pos_state[1]
            moves = pos_state[2]
    return mx, moves
    
def solve_for_one_unit(board, start_message, start_points, unit, unit_pos, ls_old): # return list of (board, move_score, moves, number of cleared lines)
    #here we make BFS
    ans = []
    was = set()
    q = deque()
    q.append((unit_pos, start_message))
    was.add(pos_to_str(unit_pos))
    for additional_angel in unit.angle_sim:
        was.add(pos_to_str((unit_pos[0], (unit_pos[1] + additional_angel) % 360)))
    while len(q) > 0:
        cur_pos, cmds = q.popleft()
        for mv, symbol in move:
            new_pos = mv(cur_pos)
            if pos_to_str(new_pos) in was:
                continue
            was.add(pos_to_str(new_pos))
            for additional_angel in unit.angle_sim:
                was.add(pos_to_str((new_pos[0], (new_pos[1] + additional_angel) % 360) ))
            if board.is_allowed(unit, new_pos):
                q.append((new_pos, cmds + symbol))
            else:
                new_board = Board.add_unit(board, unit, cur_pos)
                ls = new_board.clear_lines()
                points = len(unit.members) + 100 * (1 + ls) * ls / 2
                if ls_old > 1: points = points + int(Math.floor((ls_old - 1) * points / 10))
                ans.append((new_board, start_points + points, cmds + symbol, ls))
    return ans