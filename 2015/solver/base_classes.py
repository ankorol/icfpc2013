# Unit position is a tuple (cell, angle), where
# cell - coordinate of pivot point
# angle - angle of rotation of unit counter-clockwise beginning from East 
#
#               120  60
#            180   .    0
#               240  300
#

def to_cube(row, column):
    x = column - (row - (row % 2)) / 2
    z = row 
    y = -x - z
    return x, y, z

def to_cell(x, y, z):
    return Cell(z, x + (z- (z % 2))/ 2)
 
def rotate_cube(x, y, z):
    return -y, -z, -x
 
def rotate(cell):
    x, y, z = to_cube(cell.row, cell.column)
    x, y, z = rotate_cube(x, y, z)
    return to_cell(x, y, z)

class Cell:
    def __init__(self, row, column):
        self.row = row
        self.column = column
    
    @classmethod
    def from_dict(cls, parsed_cell_dict):
        return cls(parsed_cell_dict['y'], parsed_cell_dict['x'])
    
    def to_string(self):
        return str(self.row) + " " + str(self.column)

class Unit:
    def __init__(self, parsed_unit_dict, board_height, board_width):
        self.pivot = Cell.from_dict(parsed_unit_dict['pivot'])
        self.members = [Cell.from_dict(m) for m in parsed_unit_dict['members']]
        self.is_normalized = self.__normalize(board_height, board_width)
        self.angle_sim = []
        if self.is_normalized:
            cells = set()
            for m in self.members:
                cells.add(m.to_string())
            for angle in (60, 120, 180):
                adj = self.get_adjusted_members((self.pivot, angle))
                sim = True
                for am in adj:
                    if am.to_string() not in cells:
                        sim = False
                        break
                if sim:
                    self.angle_sim += range(angle, 360, angle)
                if len(self.angle_sim) != 0:
                    break
    
    def __normalize(self, board_height, board_width):
        topmost = self.members[0].row
        bottommost = self.members[0].row
        leftmost = self.members[0].column
        rightmost = self.members[0].column
        for m in self.members:
            if m.row < topmost: topmost = m.row
            if m.row > bottommost: bottommost = m.row
            if m.column < leftmost: leftmost = m.column
            if m.column > rightmost: rightmost = m.column
        if (bottommost - topmost + 1 > board_height): return False
        if (rightmost - leftmost + 1 > board_width): return False
        delta = (board_width - (rightmost - leftmost + 1)) / 2 - leftmost;
        for m in self.members:
            m.row -= topmost
            m.column += delta
        self.pivot.row -= topmost
        self.pivot.column += delta
        return True
    
    def get_adjusted_members(self, unit_pos):
        ans = []
        for m in self.members:
            #move pivot to (0, 0) rotate and then move pivot to unit_pos cell
            cell = Cell(m.row - self.pivot.row, m.column - self.pivot.column)
            for i in xrange(unit_pos[1] / 60):
                cell = rotate(cell)
            cell.row += unit_pos[0].row
            cell.column += unit_pos[0].column
            ans.append(cell)
        return ans
        
class Board:

    def __init__(self, height, width, matrix):
        self.height = height
        self.width = width
        self.matrix = matrix
    
    @classmethod
    def from_dict(cls, height, width, filled):
        matrix = [[0] * width for i in xrange(height) ]
        for cell in filled:
            matrix[cell['y']][cell['x']] = 1
        return cls(height, width, matrix)
    
    @classmethod
    def add_unit(cls, board, unit, unit_pos):
        matrix = [i[:] for i in board.matrix]
        new_board = cls(board.height, board.width, matrix)
        mbrs = unit.get_adjusted_members(unit_pos)
        for m in mbrs:
            new_board.matrix[m.row][m.column] = 1
        return new_board
        
    def is_allowed(self, unit, unit_pos):
        if not unit.is_normalized: return False
        mbrs = unit.get_adjusted_members(unit_pos)
        for m in mbrs:
            if m.row < 0 or m.row >= self.height: return False
            if m.column < 0 or m.column >= self.width: return False
            if self.matrix[m.row][m.column] == 1: return False
        return True
    
    def clear_lines(self):
        ls = 0
        current_line_to_fill = self.height - 1
        for i in reversed(xrange(self.height)):
            if self.matrix[i].count(1) != self.width:
                self.matrix[current_line_to_fill] = self.matrix[i][:]
                current_line_to_fill -= 1
            else:
                ls += 1
        while current_line_to_fill >= 0:
            self.matrix[current_line_to_fill] = [0] * self.width
            current_line_to_fill -= 1
        return ls