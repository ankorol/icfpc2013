from bvlang import *


mainParam = BVId('x')
#t = BVProgram(mainParam, BVExprIf(BVExprID(mainParam), BV1, BVExprOp2(PLUS, BVExprID(mainParam), BVExprOp1(SHL1, BVExprID(mainParam)) ) ) )
t = BVProgram(mainParam, BVExprOp2(PLUS, BVExprID(mainParam), BVExprOp1(NOT, BVExprID(mainParam)))  ) 
t = BVProgram(mainParam, BVExprFold(IDX, BV0, IDY, IDZ, BVExprOp1(SHR1, BVExprOp1(NOT, IDY) ) ) ) 
#t = BVProgram(mainParam, BV1)

print t.toCode()

d = ["0x26B37576A1BD5818", "0xB3DB2E3637FD9D19", "0x1410F6998292EB72", "0x7C18B12D0DF650A3", "0xD074A21A853B2E23", "0x4BC5E3CE1F182C4B", "0xF4650D6654774A4E", "0xF70CB2508AA72EF9", "0x774F2DFE95D75B4B", "0xCF08BDF822CF6A1A"]
for i in d:
	print "f(%s) = %s" % (i, str(hex(t.calc(int(i,16)))) )
