from bvlang import *

def FillerNOT(ex, dest, level, mex=None):
  prev  = ex[level-1]
  for expr in prev:
    dest.append(BVExprOp1(NOT, expr))
def FillerSHL1(ex, dest, level, mex=None):
  prev  = ex[level-1]
  for expr in prev:
    dest.append(BVExprOp1(SHL1, expr))
def FillerSHR1(ex, dest, level, mex=None):
  prev  = ex[level-1]
  for expr in prev:
    dest.append(BVExprOp1(SHR1, expr))
def FillerSHR4(ex, dest, level, mex=None):
  prev  = ex[level-1]
  for expr in prev:
    dest.append(BVExprOp1(SHR4, expr))
def FillerSHR16(ex, dest, level, mex=None):
  prev  = ex[level-1]
  for expr in prev:
    dest.append(BVExprOp1(SHR16, expr))

def mfFillerOp2(mfold, dest, level, mex, theOp):
  level-=1  # na sam operator
  for e1 in xrange(3, level):
    e2 = level - e1 - 1
    expr1 = mfold[e1]
    expr2 = mex[e2]
    for a1 in expr1:
      for a2 in expr2:
        dest.append(BVExprOp2(theOp, a1, a2))
def mfFillerAND(mfold, dest, level, mex):
  mfFillerOp2(mfold, dest, level, mex, AND);
def mfFillerOR(mfold, dest, level, mex):
  mfFillerOp2(mfold, dest, level, mex, OR);
def mfFillerXOR(mfold, dest, level, mex):
  mfFillerOp2(mfold, dest, level, mex, XOR);
def mfFillerPLUS(mfold, dest, level, mex):
  mfFillerOp2(mfold, dest, level, mex, PLUS);

def mfFillerIF0(mfold, dest, level, mex):
  level-=1
#  print "if0 level", level, "mex len "
  ml = len(mex)
  for i in xrange(level):
    for j in xrange(level):
      k = level-2-i-j
      if k>=0:
        if j<ml and k<ml:
          pr1 = mfold[i]
          pr2 = mex[j]
          pr3 = mex[k]
          for expr1 in pr1:
            for expr2 in pr2:
              for expr3 in pr3:
                dest.append(BVExprIf(expr1, expr2, expr3))
        if i<ml and k<ml:
          pr1 = mex[i]
          pr2 = mfold[j]
          pr3 = mex[k]
          for expr1 in pr1:
            for expr2 in pr2:
              for expr3 in pr3:
                dest.append(BVExprIf(expr1, expr2, expr3))
        if i<ml and j<ml:
          pr1 = mex[i]
          pr2 = mex[j]
          pr3 = mfold[k]
          for expr1 in pr1:
            for expr2 in pr2:
              for expr3 in pr3:
                dest.append(BVExprIf(expr1, expr2, expr3))

def FillerAND(ex, dest, level):
  for i in xrange((level-1)/2):     
    prev1  = ex[level-2-i]
    prev2  = ex[i]
    for first in prev1:
      for second in prev2:
        dest.append(BVExprOp2(AND, first, second))
  if (level-2)&1==0:    
    prev = ex[(level-2)>>1]
    for i in xrange(len(prev)):
      for j in xrange(i, len(prev)):
        dest.append(BVExprOp2(AND, prev[i], prev[j]))
def FillerOR(ex, dest, level):
  for i in xrange((level-1)/2):     
    prev1  = ex[level-2-i]
    prev2  = ex[i]
    for first in prev1:
      for second in prev2:
        dest.append(BVExprOp2(OR, first, second))
  if (level-2)&1==0:    
    prev = ex[(level-2)>>1]
    for i in xrange(len(prev)):
      for j in xrange(i, len(prev)):
        dest.append(BVExprOp2(OR, prev[i], prev[j]))
def FillerXOR(ex, dest, level):
  for i in xrange((level-1)/2):     
    prev1  = ex[level-2-i]
    prev2  = ex[i]
    for first in prev1:
      for second in prev2:
        dest.append(BVExprOp2(XOR, first, second))
  if (level-2)&1==0:    
    prev = ex[(level-2)>>1]
    for i in xrange(len(prev)):
      for j in xrange(i, len(prev)):
        dest.append(BVExprOp2(XOR, prev[i], prev[j]))
#TODO uzau PLUS dlia debaga binarnyh operatorov! ostal'nye eto copy-paste
def FillerPLUS(ex, dest, level):
  for i in xrange((level-1)/2):     #TODO: pereproverit' granicu!! VEROYATNYI BAG! esli
    prev1  = ex[level-2-i]
    prev2  = ex[i]
    for first in prev1:
      for second in prev2:
#        print "otot ", BVExprOp2(PLUS, first, second).toCode()
        dest.append(BVExprOp2(PLUS, first, second))
  #proveriaem slu4ai kogda oba argumenta odinakovoi dlinny, i mi berem ih iz odnogo i togoje massiva
  if (level-2)&1==0:    
    prev = ex[(level-2)>>1]
    for i in xrange(len(prev)):
      for j in xrange(i, len(prev)):
#        print "etot ", BVExprOp2(PLUS, prev[i], prev[j]).toCode()
        dest.append(BVExprOp2(PLUS, prev[i], prev[j]))


def FillerIF0(ex, dest, level):
  for i in xrange(level-1):
    for j in xrange(level-1):
      k = level-3-i-j
      if k>=0:
        pr1 = ex[i]
        pr2 = ex[j]
        pr3 = ex[k]
        for expr1 in pr1:
          for expr2 in pr2:
            for expr3 in pr3:
              dest.append(BVExprIf(expr1, expr2, expr3))

opFiller = {
"not":FillerNOT,
"shl1":FillerSHL1,
"shr1":FillerSHR1,
"shr4":FillerSHR4,
"shr16":FillerSHR16,

"and":FillerAND,
"or":FillerOR,
"xor":FillerXOR,
"plus":FillerPLUS,

"if0":FillerIF0
}

mFoldOpFiller = {
"not":FillerNOT,  #esli peredat' pravil'nye parametry to metody OP1 nam vse podhodiat
"shl1":FillerSHL1,
"shr1":FillerSHR1,
"shr4":FillerSHR4,
"shr16":FillerSHR16,

"and":mfFillerAND,
"or":mfFillerOR,
"xor":mfFillerXOR,
"plus":mfFillerPLUS,

"if0":mfFillerIF0
}

class Podbiralka:
  cntx = {'x':5}
  def prepareExMap(self, sizeLimit, allowedOps, atoms):
    ex = [[] for i in xrange(sizeLimit)]
    ex[0] = atoms
    for i in xrange(1, sizeLimit):
      print "Generim funkcii razmera ", i+2   #+1 izza togo 4to s nulevym indeksom idut expressiony dlinoi 1, i ewe +1 izza togo 4to obertka v programmu imeet dlinnu 1
      dest = ex[i]
      for op in allowedOps:
        opFiller[op](ex, dest, i)
    return ex
  def genNoFold(self, sizeLimit, allowedOps):
    sizeLimit-=1
#      for exp in dest: print exp.toCode(), exp.evaluate(Podbiralka.cntx)
    cm = self.prepareExMap(sizeLimit, allowedOps, [BV0, BV1, IDX])
    dest = cm[sizeLimit-1]
    self.curLevel = sizeLimit-1
    self.candMap = cm
    return dest  

  def genTFold(self, sizeLimit, allowedOps):
    ao = filter(lambda x: x!="tfold", allowedOps)
    sizeLimit-=5
    level = sizeLimit
    ex2 = self.prepareExMap(sizeLimit, ao, [BV0, BV1, IDY, IDZ])
    print level-1
    mp = []
    for k in xrange(level):
      dest=[]
      for ex in ex2[k]: 
        dest.append(BVExprFold(IDX, BV0, IDY, IDZ, ex))
      mp.append(dest)
    self.candMap = mp
    self.curLevel = sizeLimit-1
#    for exp in dest: print exp.toCode()
    return dest
  def genFold(self, sizeLimit, allowedOps):
    sizeLimit -=1
#    print "gf ", sizeLimit
    ao = filter(lambda x: x!="fold", allowedOps)
    level = sizeLimit-1 # otbrosili "program"
    #generim MapuExpressionov - 
    mex = self.prepareExMap(level-3, ao, [BV0, BV1, IDX]) # (level - 5) tak kak na verhnem urovne budet op potom gde-to ewe fold
    #generim MapuTFoldov, gde fold na verhnem urovne
    mex2 = self.prepareExMap(level-3, ao, [BV0, BV1, IDY, IDZ])
    mtfold = [[],[],[]]
    for l in xrange(3, level):
      tf = []
      for e1 in xrange(l-2):
        for e2 in xrange(l-2-e1):
          e3 = l-3-e1-e2
#          print e1,e2,e3
          pr1 = mex[e1]
          pr2 = mex[e2]
          pr3 = mex2[e3]
          for expr1 in pr1:
            for expr2 in pr2:
              for expr3 in pr3:
                tf.append(BVExprFold(expr1, expr2, IDY, IDZ, expr3))
#      print "tfold razm", l
#      for ee in tf: print ee.toCode()
      mtfold.append(tf)
    #generim MapuFoldov, gde est' rovno odin fold (bez ograni4eniya na verhnem on urovne ili net)
    mfold = [[],[],[]]
    for l in xrange(3, level):
      rz = list(mtfold[l])  #vklu4ili vse tfoldi
      for op in ao: #vklu4ili vse operacii
        mFoldOpFiller[op](mfold, rz, l, mex)
#      print "mfold razmera ", l
#      for dd in rz: print dd.toCode()
      mfold.append(rz)
    self.curLevel = level-1
    self.candMap = mfold
#    for r in rz: print r.toCode()
    return rz
  def __init__(self, sizeLimit, allowedOps):
    print "Generim vse vozmojnie funkcii i zabivaem na sizeLimit!!!"
    sizeLimit = 10  #mojno stavit 11!!!
    if len(allowedOps)>7: sizeLimit-=1
    if len(allowedOps)<5: sizeLimit+=1
    if len(allowedOps)<3: sizeLimit+=2

    if 'tfold' in allowedOps: self.candidates = self.genTFold(sizeLimit+2, allowedOps)
    elif 'fold' in allowedOps: self.candidates = self.genFold(sizeLimit, allowedOps)
    else: self.candidates = self.genNoFold(sizeLimit, allowedOps)

  def filter(self, par, val):
    print "filtruem po %d = %d" %(par, val)
    cand2 = []
    cntx = {"x": par}
    while self.curLevel>1:
      for expr in self.candidates: 
#        print cntx, expr.toCode(), expr.evaluate(cntx), val, expr.evaluate(cntx)==val
        if expr.evaluate(cntx)==val: cand2.append(expr)
      print "ostalos candidatov ", len(cand2)
      self.candidates = cand2
      if len(self.candidates)==0:
        self.curLevel -= 1
        print "probuem naiti rewenie sredi zada4 pomenwe, curLevel = ", self.curLevel
        self.candidates = self.candMap[self.curLevel]
      else: 
        return (len(cand2)==1, cand2[0])
    return (False, None)

if __name__=="__main__":
  p1 = Podbiralka(4, ["shl1","plus","xor","if0"])

