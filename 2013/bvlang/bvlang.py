from ctypes import *

class BVBlock:
  def toCode(self):
    return " -- Nedopustimo! -- "

class BVOp1Not(BVBlock):
  def toCode(self):
    return "not"
  def apply(self, val):
    return c_uint64(~val).value 
class BVOp1Shl1(BVBlock):
  def toCode(self):
    return "shl1"
  def apply(self, val):
    return c_uint64(val<<1).value
class BVOp1Shr1(BVBlock):
  def toCode(self):
    return "shr1"
  def apply(self, val):
    return c_uint64(val>>1).value
class BVOp1Shr4(BVBlock):
  def toCode(self):
    return "shr4"
  def apply(self, val):
    return c_uint64(val>>4 ).value
class BVOp1Shr16(BVBlock):
  def toCode(self):
    return "shr16"
  def apply(self, val):
    return c_uint64(val>>16).value

class BVOp2And(BVBlock):
  def toCode(self):
    return "and"
  def apply(self, arg1, arg2):
    return c_uint64(arg1 & arg2).value
class BVOp2Or(BVBlock):
  def toCode(self):
    return "or"
  def apply(self, arg1, arg2):
    return c_uint64(arg1 | arg2).value
class BVOp2Xor(BVBlock):
  def toCode(self):
    return "xor"
  def apply(self, arg1, arg2):
    return c_uint64(arg1 ^ arg2).value
class BVOp2Plus(BVBlock):
  def toCode(self):
    return "plus"
  def apply(self, arg1, arg2):
    return c_uint64(arg1 + arg2).value
 
NOT = BVOp1Not()
SHL1 = BVOp1Shl1()
SHR1 = BVOp1Shr1()
SHR4 = BVOp1Shr4()
SHR16 = BVOp1Shr16()
AND = BVOp2And()
OR = BVOp2Or()
XOR = BVOp2Xor()
PLUS = BVOp2Plus()

class BVProgram(BVBlock):
  def __init__(self, bvId, bvE):
    self.bvId = bvId
    self.bvE =bvE
  def toCode(self):
    return "( lambda (%s) %s )" % (self.bvId.toCode(), self.bvE.toCode())
  def calc(self, val):
    return self.bvE.evaluate({self.bvId.toCode(): val})

class BVExpr(BVBlock):
  def isConst(self):
    return hasattr(self, 'constVal')
  def getConstValue(self):
    return self.constVal
  def setConstValue(self, val):
    self.constVal = val

  def isCached(self, cntx):
#    return False
    return hasattr(self, 'cachePar') and (self.cachePar == cntx)
  def getCached(self):
#    print 'znachenie s kesha!!'
    return self.cachedVal
  def putCached(self, cntx, val):
    self.cachePar = cntx
    self.cachedVal = val
#    pass
  def evaluate(self, cntx):
    return "hz!"

class BVExprConst(BVExpr):
  def __init__(self, v):
    self.vl = v
    self.setConstValue(v)
  def evaluate(self, cntx):                                                          
    return self.vl
  def toCode(self):
    return str(self.vl)


BV0 = BVExprConst(0)
BV1 = BVExprConst(1)

class BVExprID(BVExpr):
  def __init__(self, bvId):
    self.bvId = bvId
  def evaluate(self, cntx):
    if self.bvId.toCode() in cntx:
      return cntx[self.bvId.toCode()]
    else:
      return " -- error -- "
  def toCode(self):
    return self.bvId.toCode()

class BVExprIf(BVExpr):
  def __init__(self, cond, op1, op2):
    self.cond = cond
    self.op1 = op1
    self.op2 = op2
    if cond.isConst():
      if cond.getConstValue()==0 and op1.isConst(): self.setConstValue(op1.getConstValue())
      if cond.getConstValue()!=0 and op2.isConst(): self.setConstValue(op2.getConstValue())
    elif op1.isConst() and op2.isConst() and op1.getConstValue()==op2.getConstValue(): self.setConstValue(op1.getConstValue())
    
  def evaluate(self, cntx):
    if self.isConst(): return self.getConstValue()
    if self.isCached(cntx): return self.getCached()
    branch = self.op2
    if self.cond.evaluate(cntx)==0:
      branch = self.op1
    rez = branch.evaluate(cntx)
    self.putCached(cntx, rez)
    return rez
  def toCode(self):                                                                  
    return "( if0 %s %s %s )" %(self.cond.toCode(), self.op1.toCode(), self.op2.toCode())

class BVExprOp1(BVExpr):
  def __init__(self, op1, bvE):
    self.op1 = op1
    self.bvE = bvE
    if bvE.isConst(): self.setConstValue(op1.apply(bvE.getConstValue() ))
  def evaluate(self, cntx):
    if self.isConst(): return self.getConstValue()
    if self.isCached(cntx): return self.getCached()
    rez = self.op1.apply(self.bvE.evaluate(cntx) )
    self.putCached(cntx, rez)
    return rez
  def toCode(self):
    return "( %s %s )" % (self.op1.toCode(), self.bvE.toCode() )

class BVExprOp2(BVExpr):
  def __init__(self, op2, bvE1, bvE2):
    self.op2 = op2
    self.bvE1 = bvE1
    self.bvE2 = bvE2
    if bvE1.isConst() and bvE2.isConst(): self.setConstValue(op2.apply(bvE1.getConstValue(), bvE2.getConstValue() ) )
    if op2 == XOR and bvE1==bvE2: self.setConstValue(0)

  def evaluate(self, cntx):
    if self.isConst(): return self.getConstValue()
    if self.isCached(cntx): return self.getCached()
    rez = self.op2.apply(self.bvE1.evaluate(cntx), self.bvE2.evaluate(cntx) )
    self.putCached(cntx, rez)
    return rez
  def toCode(self):
    return "( %s %s %s )" % (self.op2.toCode(), self.bvE1.toCode(), self.bvE2.toCode() )


class BVId(BVBlock):
  def __init__(self, olo=None):
    self.olo = olo
  def toCode(self):
    return self.olo

IDX = BVExprID( BVId('x') )
IDY = BVExprID( BVId('y') )
IDZ = BVExprID( BVId('z') )

class BVExprFold(BVExpr):
  def __init__(self, e0, e1, bvIdY, bvIdZ, e2):
    self.e0=e0
    self.e1=e1
    self.bvIdZ = bvIdZ
    self.bvIdY = bvIdY
    self.e2 = e2
    if e0.isConst() and e1.isConst():
      self.setConstValue(self.evaluate({}))
  def toCode(self):
    return "(fold %s %s (lambda (%s %s) %s))" % (self.e0.toCode(), self.e1.toCode(), self.bvIdY.toCode(), self.bvIdZ.toCode(), self.e2.toCode() )
  def evaluate(self, cntx):
    if self.isConst(): return self.getConstValue()
    y = self.e0.evaluate(cntx)
    z = self.e1.evaluate(cntx)
    for i in xrange(0, 8):
      ar = y & 0xFF
      y>>=8
      cnt2 = {"y":ar, "z":z}
#      print cnt2
#      print "ev", ar
      z = self.e2.evaluate(cnt2)
    return z
