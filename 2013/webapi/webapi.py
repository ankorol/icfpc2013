import httplib, urllib, json
from apitypes import *
import time

def createArgs(args):
  return ["0x{0:016X}".format(x) for x in args]

def createWebApi():
  return WebApi('icfpc2013.cloudapp.net', '0495ZqtxvkaZOSxOBBWm5m8noyVAUv8IQCfBYBzvvpsH1H')

class WebApi:
  def __init__(self, host, authKey):
    self.params = urllib.urlencode({'auth': authKey})
    self.host = host
    self.authKey = authKey

  def makeRequest(self, command, obj_hook, jsonStr = ''):
    print "JSON to send = " + jsonStr
    while True:
      connection = httplib.HTTPConnection(self.host)
      connection.request("POST", "/" + command + "?auth=" + self.authKey, jsonStr)
      res = connection.getresponse()
      connection.close()
      if res.status != 200:
        if res.status == 429: 
          print "Too many request, waiting 5 secs"
          time.sleep(5)
          continue
        raise Exception("{0} {1}".format(res.status, res.reason))
      break
    return json.load(res, object_hook = obj_hook)

  def myTRAINproblems(self):
    pr = self.train(size=20, operators=['fold'])
    print "otvet --> ", pr.challenge
    return [pr]

  def myproblems(self):
    '''
    Return a list of ProblemInfo. Raise an error if response.code != 200
    '''
    return self.makeRequest('myproblems', lambda x: ProblemInfo(x))

  def train(self, size, operators = []):
    return self.makeRequest('train', lambda x: TrainResult(x), json.dumps({'size': size, 'operators': operators}))

  def eval(self, args, id = None, program = None):
    '''
    Return a EvalResult. Raise an error if response.code != 200
    '''
    if id is not None and program is not None:
      raise Exception("id and program can't be present both")
    if id is None and program is None:
      raise Exception("one of id or program should be present")
    req = {}
    if id is not None:
      req['id'] = id
    else:
      req['program'] = program
    req['arguments'] = createArgs(args)
    return self.makeRequest('eval', lambda x: EvalResult(x), json.dumps(req))

  def guess(self, id, program):
    return self.makeRequest('guess', lambda x: GuessResult(x), json.dumps({'id': id, 'program': program}))

  def status(self):
    pass