class ProblemInfo:
  def __init__(self, dic):
    assert 'id' in dic
    assert 'operators' in dic
    assert 'size' in dic
    self.id = dic['id']
    self.operators = dic['operators']
    self.size = int(dic['size'])
    self.solved = 'not tried'
    if 'solved' in dic:
      self.solved = dic['solved']
      self.timeLeft = 300
    if 'timeLeft' in dic:
      self.timeLeft = int(dic['timeLeft'])
  def toString(self):
    return "%s size %d ops:%s" % (self.id, self.size, str(self.operators))

class EvalResult:
  def __init__(self, dic):
    assert 'status' in dic
    self.status = dic['status']
    if self.status == 'ok':
      assert 'outputs' in dic
      self.outputs = [int(x, 16) for x in dic['outputs']]
    else:
      assert 'message' in dic
      self.message = dic['message']

class TrainResult:
  def __init__(self, dic):
    assert 'challenge' in dic
    assert 'id' in dic
    assert 'size' in dic
    assert 'operators' in dic
    self.challenge = dic['challenge']
    self.id = dic['id']
    self.size = int(dic['size'])
    self.operators = dic['operators']
  def toString(self):
    return "%s size %d ops:%s" % (self.id, self.size, str(self.operators))

class GuessResult:
  def __init__(self, dic):
    assert 'status' in dic
    self.status = dic['status']
    if self.status == 'win': return
    if self.status == 'mismatch':
      assert 'values' in dic
      self.values = [int(x, 16) for x in dic['values']]
      return
    if self.status == 'error':
      assert 'message' in dic
      self.message = dic['message']
    else:
      raise Exception('Unexpected status')
    if 'lightning' in dic:
      self.lightning = dic['lightning']