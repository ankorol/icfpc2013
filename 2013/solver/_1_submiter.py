from ICFPC2013.webapi.webapi import *
from ICFPC2013.bvlang.podbiralka import *
import solver

def criteria(pinfo):
#  return ((len(pinfo.operators)<=4 and pinfo.size<=13) or ("tfold" in pinfo.operators and pinfo.size<=16 and len(pinfo.operators)<=4) or ("fold" in pinfo.operators and pinfo.size<=11))
  return True # probuem vsio

LOGS_FILE = "processed.txt"
processed = []

with open( LOGS_FILE, "r" ) as f:
  for line in f:
      processed.append( line.split("\n")[0] )

print "Processed problems", processed
out = open(LOGS_FILE, "a")

#allProblems = api.myproblems()
api = createWebApi()
allProblems = api.myproblems()
allProblems.reverse()

for inf in allProblems:
  if inf.id in processed: continue
  if not criteria(inf):
    print "Fail Criteria:", inf.toString()
    continue
  print "Procesim:", inf.toString()
  out.write(inf.id+'\n')
  out.flush()
  p = Podbiralka(inf.size, inf.operators)
  if not solver.SolveProblem(inf.id, p):
    print "[FAIL] Mego Baga, ne smogli rewit!!!!"
    out.write("%s ne rewilo s gotovogo preprocesinga!!!! " % (inf.id,) )
    out.flush()
  else:
    print "[ACCEPT] "
  break

print "BOLWE NE$EGO RREWAT'"    
    
