from ICFPC2013.webapi.webapi import *
from ICFPC2013.bvlang.podbiralka import *
import random
import time

def SolveTraining(size):
  api = createWebApi()
  trainRes = api.train(size)
  p = Podbiralka(size, trainRes.operators)
  random.seed(time.time())
  while True:
    numberToAsk = [random.randint(0, pow(2, 64)) for x in xrange(254)]
    numberToAsk.append(0)
    numberToAsk.append(1)
    evalRes = api.eval(numberToAsk, id = trainRes.id)
    expr = ""
    isLast = False
    for inp,out in zip(numberToAsk, evalRes.outputs):
      isLast, expr = p.filter(inp, out)
      if isLast: break
    guessRes = api.guess(trainRes.id, "(lambda (x) " + expr.toCode() + ")")
    if guessRes.status == 'win':
      print "Solved!!!!"
      return
    if guessRes.status == 'mismatch':
      isLast, expr = p.filter(guessRes.values[0], guessRes.values[1])

def SolveProblem(id, p):
  random.seed(time.time())
  api = createWebApi()
  while True:
    numberToAsk = [random.randint(0, pow(2, 64) - 1) for x in xrange(1)]
    evalRes = api.eval(numberToAsk, id = id)
    expr = ""
    isLast = False
    for inp,out in zip(numberToAsk, evalRes.outputs):
      isLast, expr = p.filter(inp, out)
      if isLast: break
    while True:
      guessRes = api.guess(id, "(lambda (x) " + expr.toCode() + ")")
      if guessRes.status == 'win':
        print "Solved!!!!"
        return True
      if guessRes.status == 'mismatch':
        isLast, expr = p.filter(guessRes.values[0], guessRes.values[1])
        if not isLast: break